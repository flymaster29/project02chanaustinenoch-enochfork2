﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public static int playerHealth;
    public static int playerLives = 3;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);
        DontDestroyOnLoad(gameObject);
    }

    void Start ()
    {
		if (PointSystem.instance != null) {
			playerLives = 1;
		}
	}

	void Update ()
    {
		if (Input.GetKeyDown (KeyCode.M)) {
			GetComponent<SaveAndLoad> ().SaveGame ();
		}
		if (PointSystem.instance == null) {
			if (playerHealth <= 0) {
				playerLives--;
				playerHealth = 100;
				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			}
			if (playerLives <= 0) {
				playerLives = 3;
				SceneManager.LoadScene (0);
			}
		} else if (PointSystem.instance != null) {
			if (playerLives <= 0) {
				playerLives = 1;
				SceneManager.LoadScene (SceneManager.GetActiveScene ().name);
			}
		}

	}

    public void TakeHealth(int amount)
    {
        playerHealth -= amount;
    }

    public void GiveHealth(int amount)
    {
        playerHealth += amount;
    }

    public void TakeLives(int amount)
    {
        playerLives -= amount;
    }
		
}
